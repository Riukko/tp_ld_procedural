using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class Room
{
    public Rect rectangle;
    public GameObject roomVisuals;

    //Constructeur with origin, width and height
    public Room(float width, float height, Vector2 coordOrigin, float widthOffset, float heightOffset)
    {
        rectangle = new Rect(coordOrigin.x - widthOffset / 2, coordOrigin.y - heightOffset / 2, width - widthOffset / 2 , height - heightOffset / 2);
    }

    //Constructor with bottom left point and top right point
    public Room(Vector2 coordBL, Vector2 coordTR, float widthOffset, float heightOffset)
    {
        float width = Mathf.Abs(coordTR.x) - Mathf.Abs(coordBL.x);
        float height = Mathf.Abs(coordTR.y) - Mathf.Abs(coordBL.y);
        rectangle = new Rect(coordTR.x - widthOffset / 2,  coordBL.y - heightOffset / 2, width - widthOffset / 2 , height - heightOffset / 2);
    }

    //Constructor with already-set rectangle
    public Room(Rect roomRectangle, float widthOffset, float heightOffset)
    {
        rectangle = roomRectangle;
        rectangle.width -= widthOffset / 2;
        rectangle.xMin += widthOffset / 2;
        rectangle.height -= heightOffset / 2;
        rectangle.yMin += heightOffset / 2;
    }

    //Spawn the prefab of the room and scale it to the right dimensions
    public void spawnRoom()
    {
        roomVisuals = GameObject.Instantiate(GridBSP.Instance.roomPrefab, GridBSP.Instance.roomParentObject.transform);
        roomVisuals.transform.position = new Vector3(rectangle.center.x, rectangle.center.y, 3);
        roomVisuals.transform.localScale = new Vector3(rectangle.width, rectangle.height, 1);
        randomizeFloorMaterial();
        randomizeWallsMaterial();
    }

    public void randomizeFloorMaterial()
    {
        RoomVisuals roomVisualsInfos = roomVisuals.GetComponent<RoomVisuals>();


        //Randomize floor material based on the list given in room prefab
        Material randomMaterial = roomVisualsInfos.roomFloorMaterials[UnityEngine.Random.Range(0, roomVisualsInfos.roomFloorMaterials.Count)];
        roomVisualsInfos.floor.GetComponent<MeshRenderer>().sharedMaterial = randomMaterial;
    }

    public void randomizeWallsMaterial()
    {
        RoomVisuals roomVisualsInfos = roomVisuals.GetComponent<RoomVisuals>();
        Material randomMaterial = roomVisualsInfos.roomWallMaterials[UnityEngine.Random.Range(0, roomVisualsInfos.roomWallMaterials.Count)];
        for (int i = 0; i < roomVisualsInfos.roomWallsObject.Count; i++)
        {
            roomVisualsInfos.roomWallsObject[i].GetComponent<MeshRenderer>().sharedMaterial= randomMaterial;
        }
    }

    //Destroy the object and its representation on the scene
    public void Destroy()
    {

        GameObject.Destroy(roomVisuals);
    }
}
