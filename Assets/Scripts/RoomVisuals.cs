using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomVisuals : MonoBehaviour
{
    //Available room floor textures;
    public List<Material> roomFloorMaterials = new List<Material>();
    //Available room wall textures;
    public List<Material> roomWallMaterials = new List<Material>();

    public List<GameObject> roomWallsObject = new List<GameObject>();

    public GameObject floor;
}
