using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GridBSP : Singleton<GridBSP> {

    [Header("Map generation settings")]
    public float baseCellWidth;
    public float baseCellHeight;
    //The number of times a random cell will tried to be cut
    public int numberOfCutIterations;
    
    //The minimum height and width a cell should have, those values are meant to prevent cells to be too little
    [Range(0, 200)]
    public float minRoomSlotHeight;
    [Range(0, 320)]
    public float minRoomSlotWidth;
    //Those values will clamp the noise added to the half of the cell to cut 
    [Range(3, 6)]
    public int maxCoeffHoritontalNoise;
    [Range(2, 6)]
    public int maxCoeffVerticalNoise;

    //Each room created in a roomslot has a padding in order to not be exactly the same width of slot
    [Range(0f, 15f)]
    public float roomVerticalPadding;
    [Range(0f, 15f)]
    public float roomHoritontalPadding;
    //At each iterations, there is a fixed amount of tries to cut a random cell.
    //Of course the more cell there will already be, the less is the chance to cut another one
    //It is meant to have more variations in the disposition of the map
    public int numberOfTriesEachIterations;
    //If this value is set on 0, when two rooms are next to each other,
    //they will be considered as neighbours and a door will be created between them, regardless of the space available on the wall to create it
    public float minimumSpaceToCreateDoor;
    public GameObject roomPrefab;
    public GameObject roomParentObject;

    //Debug values
    [Header("Debug settings")]
    public bool gridFinished = false;
    public List<GameObject> debugTMPList;
    public Canvas canvas;
    public GameObject debugTMP;
    public bool debugRooms;
    public bool debugRoomLists;
    public bool debugRoomNeighbours;
    public bool fillWithRooms;
    public List<RoomSlot> roomSlotsList = new List<RoomSlot>();
    public List<Room> roomList = new List<Room>();



    //Draw the roomSlots rectangles with DrawLines
    public void DrawGrid()
    {
        for (int i = 0; i < roomSlotsList.Count; i++)
        {
            //Rectangles are built like this : 

            /*  D * * * * * * * * * * * * * * C
                  *                         *
                  *                         *
                  *                         *
                  *                         *
                  *                         *
                  *                         *
                A * * * * * * * * * * * * * * B
            */ 

            //A->B
            Debug.DrawLine(roomSlotsList[i].roomSlotRect.min, new Vector2(roomSlotsList[i].roomSlotRect.xMax, roomSlotsList[i].roomSlotRect.yMin), Color.red);
            //A->D
            Debug.DrawLine(roomSlotsList[i].roomSlotRect.min, new Vector2(roomSlotsList[i].roomSlotRect.xMin, roomSlotsList[i].roomSlotRect.yMax), Color.white);
            //C->D
            Debug.DrawLine(roomSlotsList[i].roomSlotRect.max, new Vector2(roomSlotsList[i].roomSlotRect.xMin, roomSlotsList[i].roomSlotRect.yMax), Color.green);
            //C->B
            Debug.DrawLine(roomSlotsList[i].roomSlotRect.max, new Vector2(roomSlotsList[i].roomSlotRect.xMax, roomSlotsList[i].roomSlotRect.yMin), Color.cyan);
        }
    }

    private void Update()
    { 
        if (roomSlotsList != null) { 
            //Draw the debug Grid
            DrawGrid();

            if (!gridFinished)
            {
                CutGrid();
                gridFinished = true;
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                ResetGrid();
                gridFinished = false;
            }
        }
    }

    private void Start()
    {
        //Starting rectangle of size baseWidth x baseHeight
        roomSlotsList.Add(new RoomSlot(baseCellWidth, baseCellHeight, new Vector2(-baseCellWidth/2, -baseCellHeight/2)));
        if (debugRooms) DebugRoomSlotTMP(); 
    }

    //Try to cut the cells a given amount of times
    private void CutGrid()
    {
        for (int i = 0; i < numberOfCutIterations; i++)
        {
            //Heads or tail to choose wether the cutting of the cell is vertical or horizontal
            bool vertical = Random.value > 0.5 ? true : false;

            //We try a certain amount of times per iteration because as we're basing our cuts on random, it is possible to not 
            // cut at all sometimes. Breaking the loop after a certain amount of times will cause certain iterations to not cut at all, 
            // giving a more wide variety of possibility for the cuts. Increasing the numbers of tries before breaking the loop will cause some
            // rooms to be smaller.
            int numberOfTry = 0;
            while (true)
            {
                //Picking a random index in the list to choose a cell to cut in half
                int randomIndex = Random.Range(0, roomSlotsList.Count);

                //We check each time if the cell is cuttable or not, based on its size (we don't want to cut below a given size)
                if (roomSlotsList[randomIndex].CheckIfCuttable(vertical))
                {
                    roomSlotsList[randomIndex].CutHalf(vertical);
                    if (debugRooms) DebugRoomSlotTMP();
                    break;
                }
                else if (roomSlotsList[randomIndex].CheckIfCuttable(!vertical))
                {
                    roomSlotsList[randomIndex].CutHalf(!vertical);
                    if (debugRooms) DebugRoomSlotTMP();
                    break;
                }
                else if (numberOfTry > numberOfTriesEachIterations)
                {
                    break;
                }
                numberOfTry++;
            }
        }

        CheckAllNeighbours();
    }


    //Reset grid to the initial starting rectangle
    public void ResetGrid()
    {
        for (int i = 0; i < roomSlotsList.Count; i++)
        {
            roomSlotsList[i].Destroy();
        }
        roomSlotsList = new List<RoomSlot>();
        roomSlotsList.Add(new RoomSlot(baseCellWidth, baseCellHeight, new Vector2(-baseCellWidth / 2, -baseCellHeight / 2)));
        if (debugRooms) DebugRoomSlotTMP();
    }


    //Check the neighbours of each cell and put them in a list as contained in each roomslot
    void CheckAllNeighbours()
    {
        for (int i = 0; i < roomSlotsList.Count; i++)
        {
            roomSlotsList[i].CheckNeighbours();
        }
    }


    //-------------------------------------------Debug functions-------------------------------------------------//
    public void DebugRoomSlotList()
    {
        Debug.Log("------------------------------");
        string debugString = "List of RoomSlot Ids : ";
        Debug.Log("Number of RoomSlots in List : " + roomSlotsList.Count);
        foreach (var roomSlot in roomSlotsList)
        {
            debugString += roomSlot.id.ToString() + " - ";
        }
        Debug.Log(debugString);
    }

    public void DebugRoomList()
    {
        Debug.Log("------------------------------");
        Debug.Log("Number of Rooms in List : " + roomList.Count);
    }

    void DebugRoomSlotTMP()
    {
        ClearDebugTMP();

        for (int i = 0; i < roomSlotsList.Count; i++)
        {
            GameObject debugTMPcopy = Instantiate(debugTMP, canvas.transform);
            debugTMPList.Add(debugTMPcopy);
            debugTMPcopy.GetComponent<TextMeshProUGUI>().text = roomSlotsList[i].id.ToString();
            debugTMPcopy.transform.position = roomSlotsList[i].roomSlotRect.center;
            debugTMPcopy.SetActive(true);
        }
    }

    void ClearDebugTMP()
    {
        foreach (var tmpObject in debugTMPList)
        {
            GameObject.Destroy(tmpObject);
        }
        debugTMPList = new List<GameObject>();
    }

}
