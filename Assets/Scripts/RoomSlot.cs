using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[Serializable]
public class RoomSlot
{
    //Each room and roomslots coordinates is stored in a Rect object
    public Rect roomSlotRect;

    public Room room;
    public int id;

    public Vector2 rectMin;
    public Vector2 rectMax;

    public List<int> neighboursSlotsIds = new List<int>(); 

    public RoomSlot()
    {

    }

    public RoomSlot(Rect rect)
    {
        roomSlotRect = rect;
    }

    public RoomSlot(float width, float height, Vector2 origin)
    {
        roomSlotRect = new Rect(origin.x, origin.y, width, height);
    }

    //DeepCopy of an existing RoomSlot to prevent shallow copies with direct references to copied instance's values
    public RoomSlot DeepCopy()
    {
        RoomSlot newSlot = new RoomSlot();
        newSlot.roomSlotRect = this.roomSlotRect;

        return newSlot;
    }


    public void CutHalf(bool vertical)
    {
        RoomSlot slotOne = this.DeepCopy();
        RoomSlot slotTwo = this.DeepCopy();
        float noise;

        //Vertical or horizontal cut
        if (vertical)
        {
            //The noise value offsets the cutting of the cell from the center of it. It is picked between 0 and the max value to add to the center in
            // order to cut the cell X times, X being the value "MaxCoeffVerticalNoise"
            noise = Mathf.Round(UnityEngine.Random.Range(0f, slotOne.roomSlotRect.width / GridBSP.Instance.maxCoeffVerticalNoise) * (UnityEngine.Random.value > 0.5 ? -1 : 1));

            //Create the new slots by cutting in half the current cell and adding noise to offset the cut
            slotOne.roomSlotRect.max = new Vector2(slotOne.roomSlotRect.center.x + noise, slotOne.roomSlotRect.yMax);
            slotTwo.roomSlotRect.min = new Vector2(slotOne.roomSlotRect.xMax, slotTwo.roomSlotRect.yMin);
        }
        else
        {
            //The noise value offsets the cutting of the cell from the center of it. It is picked between 0 and the max value to add to the center in
            // order to cut the cell Y times, Y being the value "MaxCoeffHorizontalNoise"
            noise = Mathf.Round(UnityEngine.Random.Range(0f, slotOne.roomSlotRect.height / GridBSP.Instance.maxCoeffHoritontalNoise) * (UnityEngine.Random.value > 0.5 ? -1 : 1));

            //Create the new slots by cutting in half the current cell and adding noise to offset the cut
            slotOne.roomSlotRect.max = new Vector2(slotOne.roomSlotRect.xMax, slotOne.roomSlotRect.center.y + noise);
            slotTwo.roomSlotRect.min = new Vector2(slotTwo.roomSlotRect.xMin, slotOne.roomSlotRect.yMax);
        }

        //Fill the slots with room and add them to the room list if the box is ticked in the debug settings
        if (GridBSP.Instance.fillWithRooms)
        {
            //if (GridBSM.Instance.debugRooms) Debug.Log("Noise : " + noise);
            //Creating a new room for each roomslots
            slotOne.room = new Room(slotOne.roomSlotRect, GridBSP.Instance.roomHoritontalPadding, GridBSP.Instance.roomVerticalPadding);
            slotTwo.room = new Room(slotTwo.roomSlotRect, GridBSP.Instance.roomHoritontalPadding, GridBSP.Instance.roomVerticalPadding);

            //Spawning the room visuals
            slotOne.room.spawnRoom();
            slotTwo.room.spawnRoom();

            //Modifying the lists of the GridBSM class (Adding both new slots, removing the one that was before the cut)
            GridBSP.Instance.roomList.Add(slotOne.room);
            GridBSP.Instance.roomList.Add(slotTwo.room);
        }

        GridBSP.Instance.roomSlotsList.Add(slotOne);
        GridBSP.Instance.roomSlotsList.Add(slotTwo);
        GridBSP.Instance.roomSlotsList.Remove(GridBSP.Instance.roomSlotsList.Find(x => x == this));

        //Assigning ids to the newly created slots (respectively the old slot's id for the new first slot, and the last id possible for the second one)
        slotOne.id = this.id;
        slotTwo.id = GridBSP.Instance.roomSlotsList.Count - 1;

        slotOne.rectMax = slotOne.roomSlotRect.max;
        slotOne.rectMin = slotOne.roomSlotRect.min;
        slotTwo.rectMax = slotTwo.roomSlotRect.max;
        slotTwo.rectMin = slotTwo.roomSlotRect.min;

        //Destroys the old cell that has been cut in half
        if (room != null) this.Destroy();

        //GridBSM.Instance.DebugRoomSlotList();
    }

    public bool CheckIfCuttable(bool vertical)
    {
        if (vertical)
        {
            //Debug.Log("Slot width / 2 : "+roomSlotRect.width / 2);
            return roomSlotRect.width / 2 >= GridBSP.Instance.minRoomSlotWidth;
        }
        else
        {
            //Debug.Log("Slot height / 2 : " + roomSlotRect.height / 2);
            return roomSlotRect.height / 2 >= GridBSP.Instance.minRoomSlotHeight;
        }
    }

    //Manual destroy function to ensure that the room affiliated to the slot is also destroyed
    public void Destroy()
    {
        room.Destroy();
        GridBSP.Instance.roomList.Remove(GridBSP.Instance.roomList.Find(x => x == this.room));
        room = null;
        id = -1;
    }

    //Function to check all neighbours of each cell and keep their ids in memory
    public void CheckNeighbours()
    {
        RoomSlot otherRoom;
        //We check for each room if it's a neighbour of the room we're in
        for (int i = 0; i < GridBSP.Instance.roomSlotsList.Count; i++)
        {
            otherRoom = GridBSP.Instance.roomSlotsList[i];
            //Check if the current room we're checking is a neighbour of the room we're in (and that it's not the same room)
            if (IsDirectlyNextTo(otherRoom) && otherRoom.id != id)
            {
                if (GridBSP.Instance.debugRoomNeighbours)
                {
                    Debug.DrawLine(roomSlotRect.center, otherRoom.roomSlotRect.center, Color.red, 1f);
                    Debug.Log("Room " + id + " overlaps Room " + GridBSP.Instance.roomSlotsList[i].id);
                }

                //Adding the room id to the list of neighbours
                neighboursSlotsIds.Add(otherRoom.id);
            }
        }
    }

    //Function to check if a cell is next to one another
    public bool IsDirectlyNextTo(RoomSlot otherSlot)
    {
        Rect otherRect = otherSlot.roomSlotRect;
        //First checking if the cells shares at least the same x or y on their side
        if (roomSlotRect.yMin == otherSlot.roomSlotRect.yMax
           || roomSlotRect.yMax == otherSlot.roomSlotRect.yMin)
        {
            //Then checking if the others coordinates matches the other cell as well
            if ((Tools.Between(otherRect.xMax, roomSlotRect.xMin, roomSlotRect.xMax, true) || Tools.Between(otherRect.xMin, roomSlotRect.xMin, roomSlotRect.xMax, true))
              ||(Tools.Between(roomSlotRect.xMax, otherRect.xMin, otherRect.xMax, true) || Tools.Between(roomSlotRect.xMin, otherRect.xMin, otherRect.xMax, true)))
            {
                return true;
            }
        }
        if (roomSlotRect.xMin == otherSlot.roomSlotRect.xMax
             || roomSlotRect.xMax == otherSlot.roomSlotRect.xMin)
        {
            if ((Tools.Between(otherRect.yMax, roomSlotRect.yMin, roomSlotRect.yMax, true) || Tools.Between(otherRect.yMin, roomSlotRect.yMin, roomSlotRect.yMax, true))
             || (Tools.Between(roomSlotRect.yMax, otherRect.yMin, otherRect.yMax, true) || Tools.Between(roomSlotRect.yMin, otherRect.yMin, otherRect.yMax, true)))


            {
                return true;
            }
        }

        return false;

    }



}
