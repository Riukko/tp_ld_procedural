using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tools
{
    public static bool Between(this float num, float lower, float upper, bool inclusive = false)
    {
        return inclusive
            ? lower <= num && num <= upper
            : lower < num && num < upper;
    }
}
